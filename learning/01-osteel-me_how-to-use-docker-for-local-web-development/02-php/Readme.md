In docker-compose:
    build: ./nginx/
make docker use the Dockerfile in the nginx/ dir

run: docker-compose up -d

in browser, open http://localhost

see container running: docker ps
stop it: docker stop <NAME>

Note:  by default the official Nginx image will only take files named following the pattern *.conf and under conf.d/ into account


in default.conf:
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }

the interesting line is this one?: fastcgi_pass php:9000;

