
run: docker-compose up -d

in browser, open http://localhost

Note: using same image as php:
app:
    image: php:7.0-fpm

Takes no extra space:
Besides, you'll notice that we're using the same PHP image as the php container's: this is a good practice as this image already exists and reusing it doesn't take any extra space (as opposed to using a data-only image such as busybox, as you may see in other tutorials out there).

