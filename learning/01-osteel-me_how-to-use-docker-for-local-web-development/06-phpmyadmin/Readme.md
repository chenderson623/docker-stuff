
run: docker-compose up -d

in browser, open http://localhost:8080

username: root
password: secret

Note:
docker-compse has same commands as docker, but will be limited to the local dir

ie:
docker-compose ps
docker-compose stop
docker-compose rm

Just like its equivalent docker rm, you can add the option -v if you want to remove the corresponding volumes as well (if you don't, you might end up with dangling volumes as already mentioned earlier).

This is neat too:
docker-compose logs - shows activity logs as they happen