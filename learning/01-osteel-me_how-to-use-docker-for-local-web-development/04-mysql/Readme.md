
run: docker-compose up -d

in browser, open http://localhost


added:
    links:
        - mysql
AND
    build: ./php/
 to php


see: https://hub.docker.com/_/php/ for notes about adding php extensions

Note in data container:
data:
    image: mysql:latest
    volumes:
        - /var/lib/mysql
    command: "true"

We don't define a host location - docker will take care of it.

database passwords: hard coded in index.php

Run docker ps to get mysql container id
In my case: ead4f57bbbf0

run: docker exec -it ead4f57bbbf0 /bin/bash

run: mysql -uroot -psecret
(project database is already there)

run:
$ mysql> use project
$ mysql> CREATE TABLE users (id int);

exit container with ctrl + d

Where is data stored?
find container volume (should be mysql, exited)
run docker inspect 5c0c6aad9967 > inspect

Note 'Mounts:' section:
                "Name": "fb5586d67228565f57eba02ece40d377d69b67b52905b35e71f63a8468570245",
                "Source": "/var/lib/docker/volumes/fb5586d67228565f57eba02ece40d377d69b67b52905b35e71f63a8468570245/_data",
                "Destination": "/var/lib/mysql",


