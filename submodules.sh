#!/usr/bin/env bash

whotest[0]='test' || (echo 'Failure: arrays not supported in this version of
bash.' && exit 2)

add_submodule() {
    if [ ! -d "$2" ]; then
        echo $2
        git submodule add $1 $2
    fi
}


submodules=(
'git@github.com:tutumcloud/tutum-docker-mysql.git other-docker-images/mysql/tutumcloud-tutum-docker-mysql'
'git@github.com:docker-library/mysql.git          other-docker-images/mysql/docker-library-mysql'
'git@github.com:sameersbn/docker-mysql.git        other-docker-images/mysql/sameersbn-docker-mysql'

'git@github.com:clue/docker-adminer.git           other-docker-images/adminer/clue-docker-adminer'

'git@github.com:HaronK/docker-vim.git             other-docker-images/vim/HaronK-docker-vim'
'git@github.com:bcbcarl/docker-vim.git            other-docker-images/vim/bcbcarl-docker-vim'

'https://github.com/atrauzzi/docker-laravel.git       other-dockers/laravel/atrauzzi-docker-laravel'
'https://bitbucket.org/tbirleffi/docker-laravel-homestead-light.git   other-dockers/laravel/tbirleffi-docker-laravel-homestead-light'
'git@github.com:harshjv/docker-laravel.git            other-dockers/laravel/harshjv-docker-laravel'
'git@github.com:shincoder/homestead-docker.git        other-dockers/laravel/shincoder-homestead-docker'
'https://github.com/mtmacdonald/docker-laravel.git    other-dockers/laravel/mtmacdonald-docker-laravel'
'git@github.com:tyloo/docker-laravel.git              other-dockers/laravel/tyloo-docker-laravel'

'https://github.com/Morriz/docker-for-modern-devs.git other-dockers/web/Morriz-docker-for-modern-devs'
'git@github.com:andersonamuller/project.git           other-dockers/web/andersonamuller-project' #has shipyard
'git@github.com:mblaschke/TYPO3-docker-boilerplate.git  other-dockers/web/mblaschke-TYPO3-docker-boilerplate' # chechout docker-env.yml
'git@github.com:czettnersandor/vagrant-docker-lamp.git  other-dockers/web/czettnersandor-vagrant-docker-lamp'

'git@github.com:eugene-dounar/docker-example.git      other-dockers/php/eugene-dounar-docker-example'

'git@github.com:sameersbn/docker-browser-box.git      other-dockers/gui/sameersbn-docker-browser-box'
'git@github.com:szimszon/firefox-docker.git           other-dockers/gui/szimszon-firefox-docker'

'https://github.com/atrauzzi/docker-laravel.git       other-dockers-good/atrauzzi-docker-laravel'

'git@github.com:lowescott/learning-tools.git          other-dockers/misc/lowescott-learning-tools'
)

for ((i = 0; i < ${#submodules[@]}; i++))
do

    LINE="${submodules[$i]}"
    PARTS=($LINE)
    add_submodule ${PARTS[0]} ${PARTS[1]}

done