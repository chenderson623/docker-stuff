#!/bin/bash

docker stop test_mysql_data
docker stop test_mysql_data
docker stop test_phpmyadmin

docker rm test_mysql_data
docker rm test_mysql
docker rm test_phpmyadmin
