#!/bin/bash


docker run -d -v $PWD/data:/dbstorage --name test_mysql_data chenderson623-mysql-data

docker run -d -p 33061:3306 --volumes-from test_mysql_data --privileged=true --name test_mysql chenderson623-mysql

docker run -d --link test_mysql:mysql -e MYSQL_USERNAME=admin -e MYSQL_PASSWORD=admin --name test_phpmyadmin -p 8085:80 corbinu/docker-phpmyadmin