#!/bin/bash

if [ -z "$1" ]
  then
    echo "No container id passed"
    echo
    docker ps
    echo
    exit 1
fi

#sudo docker run -i -t --entrypoint /bin/bash $1 # this one looks for image (note: run)
# sudo docker attach $1  #doesn't work - hangs
sudo docker exec -it $1 bash
